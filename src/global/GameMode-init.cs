 namespace Global {
    public class GameMode : MonoBehaviour {
    
    (...)
    
    private ReactiveProperty<WorldStates> worldState;
    public ReadOnlyReactiveProperty<WorldStates> WorldState { get; private set; }
    
    public void CreateWorld() {
        InitWorld();
        StartWorld();
    }

    private void InitWorld() {
        worldState.Value = WorldStates.Initializing;
        worldState.Value = WorldStates.Initialized;
    }

    private void StartWorld() {
        worldState.Value = WorldStates.Starting;
        worldState.Value = WorldStates.Started;
    }
    
    (...)
}
