﻿public class GameEnd : MonoBehaviour {
	(...)

    public IObservable<Unit> EndOfGameEvent { get; private set; }
	(...)

    private void Awake() {
        EndOfGameEvent = player.IsDead
            .Select(_ => {
                return new Unit();
            });

        EndOfGameEvent
            .Subscribe(_ => {
                gameMode.EndGame();
            })
            .AddTo(this);

        //obsłużenie końca gry
        gameMode.WorldState
            .Where(state => state == WorldStates.Ending)
            .Subscribe(_ => {
				// zatrzymanie czasu
                Time.timeScale = 0;
            })
            .AddTo(this);
    }
}