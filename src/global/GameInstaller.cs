﻿public class GameInstaller : MonoInstaller<GameInstaller> {
    // wystawienie pola do wstawienia GameObject tutaj akurat z obiektem gracza
    [SerializeField] private GameObject playerPrefab;
    
    (...)

    public override void InstallBindings() {
        // Funkcje bindujące pogrupowane wg. kontekstu w którym zostały użyte
        // ułatwia to nawigację
        BindGameMode(); // klasa GameMode wraz z jej zależnościami
        BindGameState(); // itd.
        BindMisc();
        BindWorldInit();
        BindItemFactory();
        BindPresenters();
        BindCraftSystem();
        BindWayPoints();
    }

    private void BindWayPoints() {
	// przykład
        // Wyszukuje w we wcześniej zdefiniowanym i prawidłowo wypełnionym obiekcie
        // Klasy typu WapointPathfinder
        // jeśli znajdzie przekazuje to do FromInstance
        // następnie inne skrypty, klasy mogą uzyskać do niego dostęp, dzięki użyciu funkcji Bind<T>
        Container.Bind<WaypointPathfinder>()
            .FromInstance(waypointsSystemObject.GetComponent<WaypointPathfinder>());
    }

    (...)
}
