public class GameMode : MonoBehaviour {
	private PlayerUtilities playerUtilities;

	[SerializeField]
	private Settings settings;
	
	// Uchwyt do zdarzenia typu zatrzymanie, wznowienie gry itp.
	private ReactiveProperty<WorldStates> worldState;
	public ReadOnlyReactiveProperty<WorldStates> WorldState { get; private set; }

	// Gracze występujący w grze
	public ReactiveCollection<PlayerPawn> Players;

	public int NumberOfPlayers { get { return Players.Count; } }

	[Inject]
	public void Construct(PlayerUtilities playerUtilities) {
		this.playerUtilities = playerUtilities;
	}

	private void Awake() {
		worldState = new ReactiveProperty<WorldStates>();
		WorldState = new ReadOnlyReactiveProperty<WorldStates>(worldState);
		Players = new ReactiveCollection<PlayerPawn>();
	}

	public void CreateWorld() {
		InitWorld();
		StartWorld();
	}

	private void InitWorld() {
		worldState.Value = WorldStates.Initializing;
		worldState.Value = WorldStates.Initialized;
	}

	private void StartWorld() {
		worldState.Value = WorldStates.Starting;
		worldState.Value = WorldStates.Started;
	}

	public void StopGame() {
		if (settings.CanBePaused && worldState.Value != WorldStates.Stopped) {
			worldState.Value = WorldStates.Stopping;
			worldState.Value = WorldStates.Stopped;
		}
	}

	public void ResumeGame() {
		if (worldState.Value == WorldStates.Stopped) {
			worldState.Value = WorldStates.Resuming;
			worldState.Value = WorldStates.Resumed;
		}
	}

	public void EndGame() {
		if (worldState.Value != WorldStates.Ended) {
			worldState.Value = WorldStates.Ending;
			worldState.Value = WorldStates.Ended;
		}
	}

	public bool CheckIfAvailablePlayerSlots() {
		return playerUtilities.CheckForAvailableSlots(NumberOfPlayers, settings.MaxNumberOfPlayers);
	}

	[Serializable]
	public class Settings {
		public bool CanBePaused = true;
		public int MaxNumberOfPlayers = 1;
	}
}

public class PlayerUtilities {
	public bool CheckForAvailableSlots(int current, int max) {
		return current < max ? true : false;
	}
}

/// <summary>
/// Enum modelujący stan gry
/// Pierwsza wartośc danego stanu oznacza czynność
/// Druga że już się zakończyła
/// </summary>
public enum WorldStates {
	Running,
	Initializing,
	Initialized,
	Starting,
	Started,
	Stopping,
	Stopped,
	Resuming,
	Resumed,
	Ending,
	Ended,
	None
}
