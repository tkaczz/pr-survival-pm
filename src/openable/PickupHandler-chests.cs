﻿public class PickupHandler : MonoBehaviour {
    private IController playerController;
	(...)
    private Subject<IOpenable> openableSubject;
    public IObservable<IOpenable> OpenableEvent { get { return openableSubject; } }
	(...)

    private void InitOpenable() {
        openableSubject = new Subject<IOpenable>();

        playerController.Use
            .Subscribe(_ => {
                var itemCheck = RaycastForObjects();

                if (itemCheck == null) { return; }

                if (itemCheck.layer == LayerMask.NameToLayer("Chest")) {
                    openableSubject.OnNext(itemCheck.GetComponent<IOpenable>());
                }
            })
            .AddTo(this);
    }
	(...)
}