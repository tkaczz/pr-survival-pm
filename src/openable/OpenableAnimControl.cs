﻿[RequireComponent(typeof(Animator))]
public class OpenableAnimControl : MonoBehaviour {
	(...)

    private void InitStates() {
		// pobranie hash parametru IsOpened
		// przez jego zmianę uruchamiana jest animacja otwierania, zamykania skrzyni
        var isOpened = Animator.StringToHash("IsOpened");

		// nasłuchiwanie na zmiany w IsOpen
		// jeśli true to wtedy otwiera
		// jeśli false to zamyka
        openable.IsOpen
            .Subscribe(state => {
                animator.SetBool(isOpened, state);
            })
            .AddTo(this);
    }
	(...)
}