﻿public interface IOpenable {
    ReadOnlyReactiveProperty<bool> IsOpen { get; }

    void Toggle();

    void Open();

    void Close();
}