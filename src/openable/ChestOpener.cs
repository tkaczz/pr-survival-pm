﻿public class ChestOpener : MonoBehaviour, IOpenable {
    private ReactiveProperty<bool> isOpen;
    public ReadOnlyReactiveProperty<bool> IsOpen { get; private set; }

    public void Close() {
        isOpen.Value = false;
    }

    public void Open() {
        isOpen.Value = true;
    }

    public void Toggle() {
        isOpen.Value = !isOpen.Value;
    }
	(...)
}