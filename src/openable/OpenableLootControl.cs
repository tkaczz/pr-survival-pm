﻿[RequireComponent(typeof(IOpenable))]
public class OpenableLootControl : MonoBehaviour {
    private IOpenable openable;
    [SerializeField]
    private GameObject lootParent;
    private GameObject[] lootItems;
	(...)

    private void InitCollidersOnStateChange() {
        openable.IsOpen
            .Where(state => state == true)
            .Subscribe(state => {
                //Debug.Log(state);
                if (lootItems == null) { return; }

                SwitchLootState(state);
                ThrowLoot();
                ClearLootList();
            })
            .AddTo(this);
    }

    private void SwitchLootState(bool state) {
        var count = lootItems.Length;

        for (int i = 0; i < count; i++) {
            var current = lootItems[i];

            if (current != null) {
                current.SetActive(state);
            }
        }
    }

    private void ClearLootList() {
        lootItems = null;
    }

    private void ThrowLoot() {
        var count = lootItems.Length;

        for (int i = 0; i < count; i++) {
            var current = lootItems[i];
            current.transform.parent = null;
        }
    }
	(...)
}