public class NumericalStat {
    private ReactiveProperty<float> current;
    public ReadOnlyReactiveProperty<float> Current { get; private set; }

    private ReactiveProperty<float> min;
    public ReadOnlyReactiveProperty<float> Min { get; private set; }

    private ReactiveProperty<float> max;
    public ReadOnlyReactiveProperty<float> Max { get; private set; }

	(...)

    public virtual void SetCurrent(float newCurrent) {
        current.Value = Mathf.Clamp(newCurrent, min.Value, max.Value);
    }

    public virtual void SetMax(float newMax) {
        max.Value = Mathf.Clamp(newMax, min.Value, max.Value + newMax);
    }

    public virtual void SetMin(float newMin) {
        min.Value = Mathf.Clamp(newMin, 0, max.Value);
    }

    /// <summary>
    /// Increments or decrements Current.Value
    /// If substracting is needed then pass -value, by default is adding
    /// </summary>
    public virtual void UnaryCurrent(float value) {
        current.Value = Mathf.Clamp(current.Value + value, min.Value, max.Value);
    }
}