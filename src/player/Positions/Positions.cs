public sealed class StandingPosition : MonoBehaviour {
	private IController pawnController;
	private IMovementController movementController;
	private RigidbodyCharacterController characterController;
	private PositionController positionController;
	private float heightMultipler = 1.0f;
	private float speedMultipler = 1.0f;

   (...)

	private void Awake() {
		pawnController.BodyPosition
			.Where(position => {
				var check =
					position == BodyPositions.Standing &&
					characterController.IsOnGround.Value == true;
				return check;
			})
			.Subscribe(position => {
				//Debug.Log("Standing");
				movementController.SpeedMultipler = speedMultipler;
				positionController.CurrentHeightMultipler.Value = heightMultipler;
			})
			.AddTo(this);
	}
}