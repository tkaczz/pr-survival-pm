public class CameraControl : MonoBehaviour {
	(...)
	private PositionController positionController;

	private void Awake() {
		(...)

		positionController.CurrentHeightMultipler
			.Subscribe(nextHeight => {
				var newCameraHeight = new Vector3(0, nextHeight, 0);

				cameraTransform.localPosition = newCameraHeight;
			})
			.AddTo(this);
	}
	(...)
}