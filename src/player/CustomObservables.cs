﻿// stworzono na bazie
// https://ornithoptergames.com/reactivex-and-unity3d-part-3/
public static IObservable<bool> Latch(
	IObservable<Unit> tick, // kiedy ma zbierać wartości
	IObservable<Unit> latchTrue, // kiedy ma publikować wartości
	bool initialValue // początkowa wartość
) {
	// Observable.Create jest na ogół używane do tworzenia nowego typu IObservable
	return Observable.Create<bool>(observer => {
		var value = initialValue;

		var latchSub = latchTrue.Subscribe(_ => value = true);

		// poniższe publikuje nowe wartości co każde zdarzenie w tick
		var tickSub = tick.Subscribe(_ => {
			observer.OnNext(value);
			value = false;
		},
			observer.OnError,
			observer.OnCompleted
		);

		return Disposable.Create(() => {
			latchSub.Dispose();
			tickSub.Dispose();
		});
	});
}