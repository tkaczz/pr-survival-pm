﻿public struct ActionInputs {
    public readonly bool Take;
    public readonly bool Use;
    public readonly bool Inventory;
    public readonly bool ExitMenu;

    public ActionInputs(
        bool take,
        bool use,
        bool inventory,
        bool exitMenu
    ) {
        this.Take = take;
        this.Use = use;
        this.Inventory = inventory;
        this.ExitMenu = exitMenu;
    }
}