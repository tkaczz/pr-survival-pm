﻿// pobranie wejścia
var jump = frameObservables.UpdateAsObservable()
	.Where(_ => Input.GetButton("Jump"));
	
// jego publikacja w co każdym FixedUpdateAsObservable
var jumpLatch = CustomObervables.Latch(
	frameObservables.FixedUpdateAsObservable(),
	jump,
	false);