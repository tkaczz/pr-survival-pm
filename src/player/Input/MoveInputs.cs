public class KeyboardMouseInputHandler : MonoBehaviour, IInput {
(...)
            MoveInputs = Observable.Zip(
                move, runLatch, jumpLatch, changePositionLatch,
                (a, b, c, d) => new MoveInputs(a, b, c, d));
(...)
}

public struct MoveInputs {
    public readonly Vector2 Movement;
    public readonly bool Run;
    public readonly bool Jump;
    public readonly bool PositionChange;

    public MoveInputs(
        Vector2 movement,
        bool run,
        bool jump,
        bool positionChange
    ) {
        this.Movement = movement;
        this.Run = run;
        this.Jump = jump;
        this.PositionChange = positionChange;
    }
}