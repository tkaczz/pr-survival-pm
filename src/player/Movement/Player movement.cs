public class PlayerMovement : MonoBehaviour, IMovementController {
	private IController controller;
	private RigidbodyCharacterController characterController;
	(...)
	private void InitMove() {
		controller.MoveVector
			.Where(_ => controlableMoving)
			.Subscribe(moveVector => {
				float run;

				if (controller.Run.Value == true) {
					run = settings.RunSpeed;
					isRunning.Value = true;
				}
				else {
					run = settings.WalkSpeed;
					isRunning.Value = false;
				}

				var vector = moveVector * run * speedMultipler;
				characterController.MoveForce(
					vector
				);
			})
			.AddTo(this);
	}
}