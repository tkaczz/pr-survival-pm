public class PlayerMovement : MonoBehaviour, IMovementController {
	private IController controller;
	private Transform playerTransform;
	private RigidbodyCharacterController characterController;
	private bool controlableMoving = true;
	private bool jumpFlag = false;

	private Subject<Unit> jumped;
	public IObservable<Unit> Jumped {
		get { return jumped; }
	}

	public IObservable<Unit> IsLanding { get; private set; }
	public ReadOnlyReactiveProperty<bool> InAir { get; private set; }

	(...)

	// dostarcza zdarzenia gdy postać znajduje się w powietrzu
	private void InitInAir() {
		InAir = characterController.IsOnGround
			.Select(isOnGround => {
				return !isOnGround;
			})
			.ToReadOnlyReactiveProperty();
	}

	private void InitLand() {
		// na ziemi, postać wcześniej skoczyła
		IsLanding = characterController.IsOnGround
			.Where(isOnGround => {
				var check =
					isOnGround == true &&
					jumpFlag == true;
				return check;
			})
			.Select(_ => {
				return new Unit();
			});

		IsLanding
			.Subscribe(_ => {
				controlableMoving = true;
				jumpFlag = false;
			})
			.AddTo(this);
	}

	private void InitJump() {
		// na ziemi, postać chce skoczyć
		jumped = new Subject<Unit>();

		controller.Jump
			.Where(_ => {
				var check =
					jumpFlag == false &&
					characterController.IsOnGround.Value == true &&
					controller.BodyPosition.Value == BodyPositions.Standing;

				(...)

				return check;
			})
			.Subscribe(_ => {
				var jumpVector = new Vector3(
					playerTransform.forward.x, 1, playerTransform.forward.z
				);

				(...)

				characterController.MoveImpulse(jumpVector * settings.JumpForce);
				jumped.OnNext(new Unit());
			})
			.AddTo(this);

		Jumped
			.Subscribe(_ => {
				controlableMoving = false;
				jumpFlag = true;
			})
			.AddTo(this);
	}
}