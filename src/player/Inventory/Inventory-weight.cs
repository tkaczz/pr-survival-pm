﻿public class Inventory : MonoBehaviour {
	(...)
    public NumericalStat Weight { get; private set; }
	(...)

    private void InitInventoryWeight() {
        Weight = new NumericalStat(0, maxInventoryWeight, 0);

        //dodawanie nowego przedmiotu
        Items
            .ObserveAdd()
            .Subscribe(newItem => {
                Weight.UnaryCurrent(CalculateItemWeight(newItem.Key, newItem.Value));
                //Debug.Log("Add " + Weight.Current.Value);
            })
            .AddTo(this);

        //usuwanie przedmiotu
        Items
            .ObserveRemove()
            .Subscribe(removedItem => {
                Weight.UnaryCurrent(-CalculateItemWeight(removedItem.Key, removedItem.Value));
                //Debug.Log("Remove " + Weight.Current.Value);
            })
            .AddTo(this);

        //zmiana ilości
        Items
            .ObserveReplace()
            .Subscribe(changedItem => {
                var diffCount = changedItem.NewValue - changedItem.OldValue;
                var diffWeight = CalculateItemWeight(changedItem.Key, diffCount);
                Weight.UnaryCurrent(diffWeight);
                //Debug.Log("Replace " + Weight.Current.Value);
            })
            .AddTo(this);
    }

	(...)
}