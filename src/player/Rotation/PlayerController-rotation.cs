public class PlayerController : MonoBehaviour, IController {
	(...)
	private IInput input;
	private Transform playerTransform;
	private Transform playerCamera;
	(...)

	public IObservable<Quaternion> XRotation { get; private set; }
	public IObservable<Quaternion> YRotation { get; private set; }

	(...)

	private void InitRotation() {
		XRotation = input.RotationVector
			.Where(inputLook => inputLook != Vector2.zero)
			.Select(inputLook => {
				// typu Vector3
				var horizontal = inputLook.x * Vector3.up;
				
				// typu Quaternion
				var horizontalQuaternion =
					playerTransform.localRotation *
					Quaternion.Euler(horizontal * Time.deltaTime);

				return horizontalQuaternion;
			});

		YRotation = input.RotationVector
			.Where(inputLook => inputLook != Vector2.zero)
			.Select(inputLook => {
				var vertical = inputLook.y * Time.deltaTime * Vector3.left;

				var verticalQuaternion =
					playerCamera.localRotation *
					Quaternion.Euler(vertical);

				// przycięcie wartości
				// by postac nie mogła się obrócić o zbyt dużą wartość
				var clamped =
					ClampRotationAroundXAxis(
						verticalQuaternion, -settings.MaxViewAngle, -settings.MinviewAngle
					);

				return clamped;
			});
	}

	// z StandardAssets
	private static Quaternion ClampRotationAroundXAxis(
		Quaternion q,
		float minAngle,
		float maxAngle
	) {
		q.x /= q.w;
		q.y /= q.w;
		q.z /= q.w;
		q.w = 1.0f;

		float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

		angleX = Mathf.Clamp(angleX, minAngle, maxAngle);

		q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

		return q;
	}
}