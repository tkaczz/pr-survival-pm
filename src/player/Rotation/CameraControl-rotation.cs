public class CameraControl : MonoBehaviour {
	(...)
	private IController pawnController;
	private Transform cameraTransform;
	(...)

	private void Awake() {
		pawnController.YRotation
			.Subscribe(quaternion => {
				// Wygładzenie rotacji
				var lerped = Quaternion.Slerp(
					cameraTransform.localRotation,
					quaternion,
					settings.InterpSpeed
				);

				cameraTransform.localRotation = lerped;
			})
			.AddTo(this);
		(...)
	}
	(...)
}