public class PlayerRotation : MonoBehaviour {
	private IController pawnController;
	private new Rigidbody rigidbody;
	(...)

	private void Start() {
		pawnController.XRotation
			.Subscribe(quaternion => {
				var lerped = Quaternion.Slerp(
					rigidbody.rotation,
					quaternion,
					settings.InterpSpeed
				);

				rigidbody.rotation = lerped;
			});
	}
	(...)
}