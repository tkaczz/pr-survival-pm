﻿public class PickupHandler : MonoBehaviour {
	(...)
    private Subject<ObjectInRangeMessage> objectInRange;

    /// <summary>
    /// Can push null
    /// </summary>
    public IObservable<ObjectInRangeMessage> ObjectInRange { get { return objectInRange; } }
	(...)

    private void InitLayers() {
        var itemsLayer = 1 << LayerMask.NameToLayer("Item");
        var chestLayer = 1 << LayerMask.NameToLayer("Chest");
        objectsLayer = itemsLayer | chestLayer;
    }

    private void InitInSight() {
        objectInRange = new Subject<ObjectInRangeMessage>();

        frameObservable.UpdateAsObservable()
            .Subscribe(_ => {
                var newItemInSight = RaycastForObjects();

                if (!ReferenceEquals(newItemInSight, currentInSightObject)) {
                    currentInSightObject = newItemInSight;

                    var objectType = GetObjectType(newItemInSight);
                    objectInRange.OnNext(new ObjectInRangeMessage(objectType, newItemInSight));
                }
            })
            .AddTo(this);
    }

    private void InitPickup() {
        pickupSubject = new Subject<Item>();

        playerController.Take
            .Subscribe(_ => {
                var itemCheck = RaycastForObjects();

                if (itemCheck == null) { return; }

                if (itemCheck.layer == LayerMask.NameToLayer("Item")) {
                    pickupSubject.OnNext(itemCheck.GetComponent<Item>());
                }
            })
            .AddTo(this);
    }

    private void InitPickup() {
        pickupSubject = new Subject<Item>();

        playerController.Take
            .Subscribe(_ => {
                var itemCheck = RaycastForObjects();

                if (itemCheck == null) { return; }

                if (itemCheck.layer == LayerMask.NameToLayer("Item")) {
                    pickupSubject.OnNext(itemCheck.GetComponent<Item>());
                }
            })
            .AddTo(this);
    }

    private GameObject RaycastForObjects() {
        RaycastHit raycastHit;

        var check = Physics.Raycast(
            playerCameraTransform.position,
            playerCameraTransform.forward,
            out raycastHit,
            pickupRayLegth,
            objectsLayer,
            QueryTriggerInteraction.Collide
        );

        if (check) {
            return raycastHit.transform.gameObject;
        }
        else {
            return null;
        }
    }
}

public enum ObjectTypes {
    Null,
    Item,
    Chest
}

public struct ObjectInRangeMessage {
    public readonly ObjectTypes ObjectType;
    public GameObject Object { get; private set; }

    public ObjectInRangeMessage(ObjectTypes objectType, GameObject @object) : this() {
        ObjectType = objectType;
        Object = @object;
    }
}