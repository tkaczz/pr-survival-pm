public class PlayerController : MonoBehaviour, IController {
	private IInput input;
	(...)
	public IObservable<Vector3> MoveVector { get; private set; }
	(...)
	private void InitMoveVector() {
		// based on https://gist.github.com/JavadocMD/b9d87c639953e19ea1943f550907b9aa
		MoveVector = input.MoveInputs
			.Where(moveInputs => moveInputs.Movement != Vector2.zero)
			.Select(moveInputs => {
				var moveVector = moveInputs.Movement;
				
				// typu Vector3
				// moveVector.x to oś Horizontal (lewo, prawo)
				// moveVector.y to oś Vertical (góra, dół)
				// playerTransform.right/forward to wektory jednostkowe w lokalnych współrzędnych
				// odpowiednio w prawo, do przodu
				var velocity =
					moveVector.x * playerTransform.right + // współrzędna x
					moveVector.y * playerTransform.forward; // współrzędna y

				return velocity;
			});
	}
	(...)
}