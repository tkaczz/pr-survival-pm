    public class RigidbodyCharacterController : MonoBehaviour {
        private FrameObservables frameObservables;
        private Rigidbody rigidBody;
        private CapsuleCollider characterCollider;
        private Transform playerTransform;

        [SerializeField]
        private float rayCastMargin = 0.1f;
        private float widthMargin;

        public ReadOnlyReactiveProperty<bool> IsOnGround { get; private set; }

        /// <summary>
        /// Różnica między dwoma pozycjami postaci z poprzedniej i następnej klatki
        /// Na sekunde, nie między Time.fixedDeltaTime
        /// </summary>
        public ReadOnlyReactiveProperty<Vector3> Velocity { get; private set; }
		(...)

        private void InitIsOnGround() {
            var maskExceptPlayer = ~(1 << LayerMask.NameToLayer("Player"));

            IsOnGround = Velocity
				// wywoływane tylko wtedy gdy będzie obserwowany ruch postaci
                .Where(velocity => velocity.sqrMagnitude > 0)
                .Select(_ => {
                    var spherePosition = new Vector3(
                            characterCollider.bounds.center.x,
                            characterCollider.bounds.center.y - (characterCollider.radius + rayCastMargin),
                            characterCollider.bounds.center.z
                        );
						
					// sprawdzenie czy collider przeciałby
					// dolną powierzchnią z którą się styka
                    var check = Physics.CheckSphere(
                            spherePosition,
                            characterCollider.radius,
                            maskExceptPlayer,
                            QueryTriggerInteraction.Ignore);
                    return check;
                })
                .ToReadOnlyReactiveProperty();
        }

		// kontrola prędkości opadania postaci
        private void InitDragCheck() {
            var inAirDrag = 0.02f;
            IsOnGround
                .Where(isOnGround => isOnGround == false)
                .Subscribe(_ => {
                    if (Mathf.Approximately(rigidBody.drag, inAirDrag) == false) {
                        rigidBody.drag = inAirDrag;
                    }
                })
                .AddTo(this);

            var onGroundDrag = 6;
            IsOnGround
                .Where(isOnGround => isOnGround == true)
                .Subscribe(_ => {
                    if (Mathf.Approximately(rigidBody.drag, onGroundDrag) == false) {
                        rigidBody.drag = onGroundDrag;
                    }
                })
                .AddTo(this);
        }

        private void InitVelocity() {
            var fixedTicksInSecond = 1 / Time.fixedDeltaTime;
			
			// pomiar pozycji z obecnej klatki
            var previousPosition = frameObservables.FixedUpdateAsObservable()
                .Select(_ => {
                    return playerTransform.position;
                })
                .ToReadOnlyReactiveProperty<Vector3>()
                .AddTo(this);
			
			// porównanie pozycji z następnej klatki, z poprzednią
            Velocity = frameObservables.FixedUpdateAsObservable()
                .SampleFrame(1)
                .Select(_ => {
                    var nextPosition = playerTransform.position;
                    var velocity = nextPosition - previousPosition.Value;
                    return velocity * fixedTicksInSecond;
                })
                .ToReadOnlyReactiveProperty<Vector3>()
                .AddTo(this);
        }

		// poniższe dwie funkcje poruszają postacią
		// w sposób ciągły, odpowiada chodzeniu
        public void MoveForce(Vector3 motion) {
            rigidBody.MovePosition(transform.position + motion);
        }

		// w sposób impulsywny, z uwzględnieniem masy obiektu
        public void MoveImpulse(Vector3 motion) {
            rigidBody.AddForce(motion, ForceMode.Impulse);
        }
    }
}