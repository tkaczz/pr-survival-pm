﻿[Serializable]
public class ItemsDataBase {
    public ItemDataRow[] Items;

    public ItemDataRow FindItemRow(ItemTypes key) {
        var count = Items.Length + 1;

        for (int i = 0; i < count; i++) {
            var current = Items[i];
            if (current.PrimaryKey == key) {
                return current;
            }
        }

        return null;
    }
}

// pełni rolę wiersza
[Serializable]
public class ItemDataRow {
    public ItemTypes PrimaryKey;
    public float Weight;

    [TextArea(1, 6)]
    public string Description;

    public Texture Thumbnail;

    public GameObject Prefab;
}