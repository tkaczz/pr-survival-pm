﻿public class CustomItemFactory : IFactory<ItemTypes, int, Item> {
	(...)

    public Item Create(ItemTypes itemType, int amount) {
		// sprawdzenie czy przedmiot znajduje się w bazie
        var itemFromDatabase = Database.FindItemRow(itemType);
		
		// jeśli jest
        if (itemFromDatabase != null) {
            var item = diContainer.InstantiatePrefabForComponent<Item>(itemFromDatabase.Prefab);
            item.ItemType = itemType;
            item.Quantity = amount;
            return item;
        }
		// jeśli nie ma
        else {
            return null;
        }
    }
}