private void InitWeight() {
	currentWeight.Text = playerInventory.Weight.Current.Value.ToString();
	maxWeight.Text = playerInventory.Weight.Max.Value.ToString();

	playerInventory.Weight.Current
		.Subscribe(newWeight => {
			currentWeight.Text = newWeight.ToString();
		})
		.AddTo(this);

	playerInventory.Weight.Max
		.Subscribe(newMaxWeight => {
			maxWeight.Text = newMaxWeight.ToString();
		})
		.AddTo(this);
}