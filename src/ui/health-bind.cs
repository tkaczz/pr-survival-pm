private void InitSubscribes() {
	SetProgressBarSubscribes(playerPawn.Health, healthBar);
}

private void SetProgressBarSubscribes(NumericalStat numStat, ProgressBar progBar) {
	numStat.Min
		.Subscribe(newMin => {
			progBar.Minimum = newMin;
		})
		.AddTo(this);

	numStat.Current
		.Subscribe(newCurrent => {
			progBar.Value = newCurrent;
		})
		.AddTo(this);

	numStat.Max
		.Subscribe(newMax => {
			progBar.Maximum = newMax;
		})
		.AddTo(this);
}