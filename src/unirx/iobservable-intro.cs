//utworzy strumień z czterech zdarzeń o odpowiednich wartościach 1, 2, 3, 4
var observable = Observable.Range(1,5);

//poniższa metoda wydrukuje na ekran 1, 2, 3, 4
observable
    .Subscribe(x => {
        Console.WriteLine(x);
    });
