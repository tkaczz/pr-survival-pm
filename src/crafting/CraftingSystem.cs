﻿public class CraftingSystem : MonoBehaviour {
    private ItemsDataBase itemsDatabase;
    private Inventory inventory;

	(...)

    public void CraftItems(RecipesDatabase.Recipe recipe) {
		// sprawdzenie składników z przedmiotami z ekwipunku
        if (CheckIngridientsWithInventory(recipe) == false) { return; }

		// tworzenie produktów z danej receptury
        foreach (var product in recipe.RecipeProducts) {
            inventory.TransferItem(product.Key, product.Value);
        }

		// zmniejszenie ilości przedmiotów w ekwipunku
		// będących składnikami receptury
        foreach (var ingridient in recipe.Ingridients) {
            inventory.DecreseItemCount(ingridient.Key, ingridient.Value);
        }
    }

    private bool CheckIngridientsWithInventory(RecipesDatabase.Recipe recipe) {
        foreach (var recipeIngridient in recipe.Ingridients) {
            //sprawdzenie czy jest taki przedmiot
            if (inventory.Items.ContainsKey(recipeIngridient.Key) == false) { return false; }

            //a potem jego ilości
            var itemCount = inventory.Items[recipeIngridient.Key] - recipeIngridient.Value;
            if (itemCount < 0) { return false; }
        }

        return true;
    }
}